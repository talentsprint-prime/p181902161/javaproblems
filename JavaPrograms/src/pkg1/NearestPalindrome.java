package pkg1;

import java.util.Scanner;

public class NearestPalindrome {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a number");
		int num = sc.nextInt();
		System.out.println("The nearest palindrome is " + calcnearPalindrome(num));
		sc.close();
	}

	public static int calcnearPalindrome(int num) {
		for (int i = 1;; i++) {
			if (isPalindrome(num + i))
				return (num + i);
		}

	}

	public static boolean isPalindrome(int x) {
		int t = x;
		int rev = 0;
		while (t > 0) {
			rev = 10 * rev + t % 10;
			t /= 10;
		}
		return rev == x;
	}

}
