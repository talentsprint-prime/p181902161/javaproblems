package pkg1;

import java.util.Scanner;

public class Aliquot {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a number");
		int num = sc.nextInt();
		System.out.println("Aliquot number is: " + generateAliquotNumber(num));
		sc.close();
	}

	public static int generateAliquotNumber(int num) {
		int sum = 0;
		for (int i = 1; i <= num; i++) {
			if (num % i == 0)
				sum = sum + i;

		}
		return sum;
	}

}



