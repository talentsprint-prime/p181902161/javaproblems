package pkg1;

import java.util.Scanner;

public class StringPalindromeCheck {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a string ");
		String str = sc.nextLine();
		System.out.println(isPalindrome(str));
		sc.close();

	}

	public static boolean isPalindrome(String str) {
		int i, j;
		boolean value = false;
		for (i = 0, j = str.length() - 1; j > i; i++, j--) {
			if (str.charAt(i) == str.charAt(j))
				value = true;
		}
		return value;
	}

}
