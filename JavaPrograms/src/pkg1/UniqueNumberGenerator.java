package pkg1;

import java.util.Scanner;

public class UniqueNumberGenerator {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the value of n");
		int n = sc.nextInt();
		int arr[] = new int[n];
		System.out.println("enter elements of array");
		for (int i = 0; i < n; i++)
			arr[i] = sc.nextInt();
		System.out.println("the sum of unique numbers in array is" + sumOfUniqueNumber(n, arr));
		sc.close();

	}

	public static int sumOfUniqueNumber(int n, int arr[]) {
		int sum = 0;
		for (int i = 0; i < n; i++) {
			int j;
			for (j = 0; j < n; j++) {
				if (i != j && arr[i] == arr[j])
					break;
			}
			if (j == n)
				sum = sum + arr[i];

		}

		return sum;
	}

}
