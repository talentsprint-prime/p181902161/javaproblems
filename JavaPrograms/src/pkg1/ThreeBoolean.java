package pkg1;

import java.util.Scanner;

public class ThreeBoolean {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean a = sc.nextBoolean();
		boolean b = sc.nextBoolean();
		boolean c = sc.nextBoolean();
		System.out.println(atleastTwo(a, b, c));
		sc.close();

	}

	public static boolean atleastTwo(boolean a, boolean b, boolean c) {
		if ((a && b) || (b && c) || (c && a))
			return true;
		else
			return false;
	}

}
